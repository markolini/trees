package trees;

/**
 * This is the helper class for a number of operations on trees.
 */
public class TreeOperations<T> {
	
	/**
	 * Takes two trees and compares them node by node if they are equal.
	 * 
	 * @param firstTree  - First tree for comparison
	 * @param secondTree - Second tree for comparison
	 * @return Are the two given trees equal
	 */
	public boolean areSame(Tree<T> firstTree, Tree<T> secondTree) {
		// Empty trees are considered equal, cause there is no Trees :)
		if (firstTree.isEmpty() && secondTree.isEmpty())
			return true;

		// If one of the trees is empty, not equal
		if (firstTree.isEmpty() || secondTree.isEmpty())
			return false;

		// Both trees have some values so lets check further
		return areSame(firstTree.left().get(), secondTree.left().get()) && areSame(firstTree.right().get(), secondTree.right().get());
	}
	
	/**
	 * Takes two trees and compares them node by node if they are isomorphic.
	 * 
	 * @param firstTree  - First tree for comparison
	 * @param secondTree - Second tree for comparison
	 * @return Are the two given trees isomorphic
	 */
	public boolean isIsomorphic(Tree<T> firstTree, Tree<T> secondTree) {
		// No trees, guess they are isomorphic then :)
		if (firstTree.isEmpty() && secondTree.isEmpty())
			return true;

		// If one of the trees is empty, not isomorphic
		if (firstTree.isEmpty() || secondTree.isEmpty())
			return false;
   
		// Check if the values of the both nodes match
		if (firstTree.value() != secondTree.value())
			return false;
		
        // Start flipping nodes to see if the trees are isomorphic
        return 
        (isIsomorphic(firstTree.left().get(), secondTree.left().get()) && isIsomorphic(firstTree.right().get(), secondTree.right().get())) || 
        (isIsomorphic(firstTree.left().get(), secondTree.right().get()) && isIsomorphic(firstTree.right().get(), secondTree.left().get())); 
    }
}
