package trees;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class TreesTest {
	
    Node<Integer> createNode(Tree<Integer> leftTree, Tree<Integer> rightTree, Integer value) {
		return new Node<Integer>(leftTree, value, rightTree);
    }

    Empty<Integer> createEmpty() {
    	return new Empty<Integer>();
    }
	
	@Test
	void shouldBeEqual() {
		Node<Integer> firstTree = createNode(
				createNode(
						createNode(
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												120),
										createNode(
												createEmpty(), 
												createEmpty(), 
												110),
										115),
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												118),
										createNode(
												createEmpty(), 
												createEmpty(), 
												116),
										104), 
								150), 
						createNode(
								createNode(
										createEmpty(),
										createEmpty(),
										112),
								createNode(
										createEmpty(),
										createEmpty(),
										116),
								140), 
						160),
				createNode(createEmpty(), createEmpty(), 250), 
				200);
		
		Node<Integer> secondTree = createNode(
				createNode(
						createNode(
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												120),
										createNode(
												createEmpty(), 
												createEmpty(), 
												110),
										115),
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												118),
										createNode(
												createEmpty(), 
												createEmpty(), 
												116),
										104), 
								150), 
						createNode(
								createNode(
										createEmpty(),
										createEmpty(),
										112),
								createNode(
										createEmpty(),
										createEmpty(),
										116),
								140), 
						160),
				createNode(createEmpty(), createEmpty(), 250), 
				200);
		
		TreeOperations<Integer> operations = new TreeOperations<>();
		boolean areTheSame = operations.areSame(firstTree, secondTree);
		assertTrue(areTheSame);
	}
	
	@Test
	void shouldNotBeEqual() {
		Node<Integer> firstTree = createNode(
				createNode(
						createNode(
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												120),
										createNode(
												createEmpty(), 
												createEmpty(), 
												110),
										115),
								createNode(
										createNode(
												createEmpty(), 
												createEmpty(), 
												118),
										createNode(
												createEmpty(), 
												createEmpty(), 
												116),
										104), 
								150), 
						createNode(
								createNode(
										createEmpty(),
										createEmpty(),
										112),
								createNode(
										createEmpty(),
										createEmpty(),
										116),
								140), 
						160),
				createNode(createEmpty(), createEmpty(), 250), 
				200);
		
		Node<Integer> secondTree = createNode(createEmpty(), createEmpty(), 200);
		
		TreeOperations<Integer> operations = new TreeOperations<>();
		boolean areTheSame = operations.areSame(firstTree, secondTree);
		assertFalse(areTheSame);
	}
	
	@Test
	void shouldBeIsomorphic() {		
		Node<Integer> firstTree = createNode(
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								120),
						createNode(
								createEmpty(), 
								createEmpty(), 
								130),
						150),
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								270), 
						createNode(
								createEmpty(), 
								createEmpty(), 
								260),
						250),
				200);
		
		Node<Integer> secondTree = createNode(
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								130),
						createNode(
								createEmpty(), 
								createEmpty(), 
								120),
						150),
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								260),
						createNode(
								createEmpty(), 
								createEmpty(), 
								290),
						250),
				200);
		
		TreeOperations<Integer> operations = new TreeOperations<>();
		boolean areIsomorphic = operations.areSame(firstTree, secondTree);
		assertTrue(areIsomorphic);
	}
	
	@Test
	void shouldNotBeIsomorphic() {
		Node<Integer> firstTree = createNode(
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								120),
						createNode(
								createEmpty(), 
								createEmpty(), 
								130),
						150),
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								270), 
						createNode(
								createEmpty(), 
								createEmpty(), 
								260),
						250),
				200);
		
		Node<Integer> secondTree = createNode(
				createNode(
						createEmpty(),
						createNode(
								createEmpty(), 
								createEmpty(), 
								120),
						150),
				createNode(
						createNode(
								createEmpty(), 
								createEmpty(), 
								260),
						createNode(
								createEmpty(), 
								createEmpty(), 
								290),
						250),
				200);
	
		TreeOperations<Integer> operations = new TreeOperations<>();
		boolean areIsomorphic = operations.areSame(firstTree, secondTree);
		assertFalse(areIsomorphic);
	}
}
